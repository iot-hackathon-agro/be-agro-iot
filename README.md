# MVP

### Setup locally 
```bash
    cp src/.env.example .env
    docker-compose up -d
    docker exec -it php /bin/sh -c "cp .env.example .env"
    docker exec -it php /bin/sh -c "composer install"
    docker exec -it php /bin/sh -c "./artisan migrate"
```

# TODO
### Knowledge
- [ ] formation templates
- [ ] NATO symbols
- [ ] message structure
### Basic Coding stuff
- [x] git repo
- [ ] consolidate or leave seperate? (**Kalvis** & **Rolands**)
- [ ] auto-deploy frontend
- [ ] auto-deploy backend
- [ ] message API agreed upon
- [ ] object API agreed upon
- [ ] template API agreed upon
### Back-end
- [ ] database of some sorts?
- [ ] receive message & save
- [ ] interpret message & save
  - [ ] ...
- [ ] notify front-end of object
- [ ] trigger fitting
- [ ] score fitness & save
  - [ ] 3-5 input-output example (**Kalvis**)
  - [ ] ...
- [ ] notify front-end of score
### Front-end
- [ ] MGRS coord system
- [ ] background map
  - [ ] change PNG to openstreetmap
  - [ ] export terrain meta from GIS via geojson
  - [ ] display terrain meta
- [ ] objects
  - [ ] prepare icon set (**Jānis**)
  - [ ] display object (**Jānis**)
- [ ] templates
  - [ ] sidepanel with score and hide/unhide checkbox
  - [ ] display template
