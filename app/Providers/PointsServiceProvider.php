<?php

declare (strict_types = 1);

namespace App\Providers;

use Ca\Domain\Model\Point\Interfaces\PointRepositoryInterface;
use Ca\Infrastructure\Model\Point\EloquentPointRepository;
use Illuminate\Support\ServiceProvider;

final class PointsServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(PointRepositoryInterface::class, EloquentPointRepository::class);
    }
}
