<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function respond(array $data = [], int $status, array $headers = []): Response
    {
        $headers = array_merge([
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin'=> '*'
        ], $headers);

        return Response::create(json_encode($data, JSON_PRETTY_PRINT), $status, $headers);
    }

    public function respondWithEmpty(int $status, array $headers = []): Response {
        $headers = array_merge([
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Origin'=> '*'
        ], $headers);

        return Response::create('', $status, $headers);
    }
}
