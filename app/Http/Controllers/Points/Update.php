<?php

declare (strict_types = 1);

namespace App\Http\Controllers\Points;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class Update extends Controller
{
    public function __invoke(Request $request): Response
    {
        dd('LocationUpdate', $request);
    }
}
