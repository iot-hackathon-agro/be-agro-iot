<?php

declare (strict_types = 1);

namespace App\Http\Controllers\Points;

use App\Http\Controllers\Controller;
use Ca\Domain\Model\Point\Id;
use Ca\Domain\Model\Point\Point;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class Demo extends Controller
{
    public function __invoke(string $id, Request $request): Response
    {
        $id     = (string) $id;
        $points = [];
        switch ($id) {
            case '1':
                $points[] = new Point(
                    new Id(1),
                    '23.97211134546,56.801679423522',
                    'recce',
                    'platoon',
                    new Carbon('2020-02-01 14:16:51'),
                    'moving NE',
                    'foe',
                    null
                );

                $points[] = new Point(
                    new Id(2),
                    '23.949776106619,56.764021512484',
                    'mechanised infantry',
                    'company',
                    new Carbon('2020-02-01 14:16:51'),
                    'moving NE',
                    'foe',
                    null
                );

                $points[] = new Point(
                    new Id(3),
                    '23.935740332946,56.754429527749',
                    'tank',
                    'platoon',
                    new Carbon('2020-02-01 14:16:51'),
                    'moving NE',
                    'foe',
                    'T-72'
                );
                break;
        }
        $response = [];
        /** @var Point $point */
        foreach ($points as $point) {
            $response[] = $point->toArray();
        }

        return $this->respond($response, 200);
    }
}
