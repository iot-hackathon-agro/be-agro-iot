<?php

declare (strict_types = 1);

namespace App\Http\Controllers\Points;

use App\Http\Controllers\Controller;
use Ca\Application\Point\Index\ListRequest;
use Ca\Application\Point\Index\ListService;
use Ca\Domain\Model\Point\Id;
use Ca\Domain\Model\Point\Point;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class Index extends Controller
{
    /**
     * @var ListService
     */
    private $service;

    public function __construct(ListService $service)
    {
        $this->service = $service;
    }

    public function __invoke(Request $request): Response
    {
        /** @var Point[] $points */
        $points = $this->service->handle(new ListRequest());
        $data   = [];
        $points = $this->getData(count($points));

        foreach ($points as $point) {
            $data[] = $point->toArray();
        }

        return $this->respond($data, 200);
    }

    private function getData(int $amount): array
    {
        $json = '[
    {
        "activity": "moving NE",
        "additional": null,
        "alignment": "foe",
        "location": [
            23.97211134546,
            56.801679423522
        ],
        "size": "platoon",
        "timestamp": 1580566611,
        "type": "recce"
    },
    {
        "activity": "moving NE",
        "additional": "T-72",
        "alignment": "foe",
        "location": [
            23.935740332946,
            56.754429527749
        ],
        "size": "platoon",
        "timestamp": 1580566611,
        "type": "tank"
    },
    {
        "activity": "moving NE",
        "additional": null,
        "alignment": "foe",
        "location": [
            23.949776106619,
            56.774021512484
        ],
        "size": "company",
        "timestamp": 1580566611,
        "type": "mechanised infantry"
    },
    {
        "activity": "moving E",
        "additional": "T-72",
        "alignment": "foe",
        "location": [
            23.885740332946,
            56.765429527749
        ],
        "size": "platoon",
        "timestamp": 1580566611,
        "type": "tank"
    }
]';

        $data   = json_decode($json, true);
        $points = [];
        for ($i = 0; $i < $amount; $i++) {
            $item     = $data[$i];
            $points[] = new Point(
                new Id($i + 1),
                $item['location'][0] . ',' . $item['location'][1],
                $item['type'],
                $item['size'],
                new Carbon($item['timestamp']),
                $item['activity'],
                $item['alignment'],
                $item['additional'] ?? null
            );
        }

        return $points;
    }
}
