<?php

namespace App\Http\Controllers\Points;

use App\Http\Controllers\Controller;
use Ca\Application\Point\Store\StoreRequest;
use Ca\Application\Point\Store\StoreService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

final class Store extends Controller
{
    private $rules = [
        'location'   => 'array|required|size:2',
        'type'       => 'string|required|in:air defence,anti-tank,engineer,hq,mechanised infantry,medics,mortars,recce,self-propelled artillery,supply,tank',
        'size'       => 'string|required|in:team,squad,platoon,company,battalion',
        'timestamp'  => 'integer|required|date_format:U',
        'activity'   => 'string|required|in:stationary,stationary in cover,moving N,moving NE,moving E,moving SE,moving S,moving SW,moving W',
        'alignment'  => 'string|required|in:friend,foe',
        'additional' => 'nullable|string|in:T-90,T-72',
    ];
    /**
     * @var StoreService
     */
    private $storeService;

    public function __construct(StoreService $storeService)
    {
        $this->storeService = $storeService;
    }

    public function __invoke(Request $request): Response
    {
        $this->validate($request, $this->rules);

        $data = $request->all();

        $point = $this->storeService->handle(
            new StoreRequest(
                $data['location'][0] . ',' . $data['location'][1],
                $data['type'],
                $data['size'],
                $data['timestamp'],
                $data['activity'],
                $data['alignment'],
                empty($data['additional']) ? null : $data['additional']
            )
        );

        return $this->respond(
            $point->toArray(),
            200
        );
    }
}
