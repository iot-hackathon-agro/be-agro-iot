<?php

declare (strict_types = 1);

namespace App\Http\Controllers\Points;

use App\Http\Controllers\Controller;
use Ca\Application\Point\Delete\DeleteRequest;
use Ca\Application\Point\Delete\DeleteService;
use Ca\Domain\Model\Point\Exceptions\PointNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class Delete extends Controller
{
    /**
     * @var DeleteService
     */
    private $deleteService;

    public function __construct(DeleteService $deleteService)
    {
        $this->deleteService = $deleteService;
    }

    public function __invoke(string $id, Request $request): Response
    {
        try {
            $this->deleteService->handle(new DeleteRequest((int) $id));
        }catch (PointNotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        return $this->respondWithEmpty(204);
    }
}
