<?php

declare (strict_types = 1);

namespace Ca\Domain\Model\Point\Interfaces;

use Ca\Domain\Model\Point\Exceptions\PointNotFoundException;
use Ca\Domain\Model\Point\Id;
use Ca\Domain\Model\Point\Point;

interface PointRepositoryInterface
{
    public function nextId(): Id;

    public function save(Point $point): void;

    /**
     * @return Point[]
     */
    public function all(): array;

    /**
     * @throws PointNotFoundException
     */
    public function delete(Id $id): void;
}
