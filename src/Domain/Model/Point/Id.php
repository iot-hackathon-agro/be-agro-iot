<?php

declare (strict_types = 1);

namespace Ca\Domain\Model\Point;

final class Id
{
    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function value(): int
    {
        return $this->id;
    }
}
