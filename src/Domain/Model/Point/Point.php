<?php

declare (strict_types = 1);

namespace Ca\Domain\Model\Point;

use DateTimeInterface;

final class Point
{
    /**
     * @var Id
     */
    private $id;
    /**
     * @var string
     */
    private $location;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $size;
    /**
     * @var DateTimeInterface
     */
    private $time;
    /**
     * @var string
     */
    private $activity;
    /**
     * @var string
     */
    private $alignment;
    /**
     * @var string|null
     */
    private $additional;

    public function __construct(
        Id $id,
        string $location,
        string $type,
        string $size,
        DateTimeInterface $time,
        string $activity,
        string $alignment,
        ?string $additional
    ) {
        $this->id         = $id;
        $this->location   = $location;
        $this->type       = $type;
        $this->size       = $size;
        $this->time       = $time;
        $this->activity   = $activity;
        $this->alignment  = $alignment;
        $this->additional = $additional;

    }

    public function id(): Id
    {
        return $this->id;
    }

    public function location(): string
    {
        return $this->location;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function size(): string
    {
        return $this->size;
    }

    public function activity(): string
    {
        return $this->activity;
    }

    public function alignment(): string
    {
        return $this->alignment;
    }

    public function additional(): ?string
    {
        return $this->additional;
    }

    public function time(): DateTimeInterface
    {
        return $this->time;
    }

    public function toArray(): array
    {
        $location = explode(',', $this->location());

        return [
            'id'         => $this->id->value(),
            'location'   => [
                (float) $location[0],
                (float) $location[1],
            ],
            'type'       => $this->type(),
            'size'       => $this->size(),
            'activity'   => $this->activity(),
            'alignment'  => $this->alignment(),
            'additional' => $this->additional(),
            'time'       => $this->time()->getTimestamp(),
        ];
    }
}
