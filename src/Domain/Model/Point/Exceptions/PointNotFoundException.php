<?php

declare (strict_types = 1);

namespace Ca\Domain\Model\Point\Exceptions;

use Ca\Domain\Model\Point\Id;
use RuntimeException;
use Throwable;

final class PointNotFoundException extends RuntimeException
{
    private function __construct(string $message, ?Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

    public static function ofId(Id $id, ?Throwable $previous = null): self
    {
        return new self(
            sprintf('Point of ID: "%s" not found.', $id->value()),
            $previous
        );
    }
}
