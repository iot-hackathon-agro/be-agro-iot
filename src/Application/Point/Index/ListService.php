<?php

declare (strict_types = 1);

namespace Ca\Application\Point\Index;

use Ca\Domain\Model\Point\Interfaces\PointRepositoryInterface;

final class ListService
{
    /**
     * @var PointRepositoryInterface
     */
    private $pointRepository;

    public function __construct(PointRepositoryInterface $pointRepository)
    {
        $this->pointRepository = $pointRepository;
    }

    public function handle(ListRequest $request): array
    {
        return $this->pointRepository->all();
    }
}
