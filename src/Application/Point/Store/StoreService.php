<?php

declare (strict_types = 1);

namespace Ca\Application\Point\Store;

use Ca\Domain\Model\Point\Interfaces\PointRepositoryInterface;
use Ca\Domain\Model\Point\Point;
use Carbon\Carbon;

final class StoreService
{
    /**
     * @var PointRepositoryInterface
     */
    private $pointRepository;

    public function __construct(PointRepositoryInterface $pointRepository)
    {
        $this->pointRepository = $pointRepository;
    }

    public function handle(StoreRequest $request): Point
    {
        $point = new Point(
            $this->pointRepository->nextId(),
            $request->location(),
            $request->type(),
            $request->size(),
            Carbon::createFromTimestamp($request->time()),
            $request->activity(),
            $request->alignment(),
            $request->additional()
        );

        $this->pointRepository->save($point);

        return $point;
    }
}
