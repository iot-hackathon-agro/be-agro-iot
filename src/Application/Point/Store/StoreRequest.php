<?php
declare (strict_types = 1);

namespace Ca\Application\Point\Store;

final class StoreRequest
{
    /**
     * @var string
     */
    private $location;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $size;
    /**
     * @var int
     */
    private $time;

    /**
     * @var string
     */
    private $activity;

    /**
     * @var string
     */
    private $alignment;

    /**
     * @var string|null
     */
    private $additional;

    public function __construct(
        string $location,
        string $type,
        string $size,
        int $time,
        string $activity,
        string $alignment,
        ?string $additional = null
    ) {
        $this->location   = $location;
        $this->type       = $type;
        $this->size       = $size;
        $this->time       = $time;
        $this->activity   = $activity;
        $this->alignment  = $alignment;
        $this->additional = $additional;
    }

    public function location(): string
    {
        return $this->location;
    }

    public function type(): string
    {
        return $this->type;
    }

    public function size(): string
    {
        return $this->size;
    }

    public function time(): int
    {
        return $this->time;
    }

    public function activity(): string
    {
        return $this->activity;
    }

    public function alignment(): string
    {
        return $this->alignment;
    }

    public function additional(): ?string
    {
        return $this->additional;
    }
}

