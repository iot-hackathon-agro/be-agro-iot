<?php

declare (strict_types = 1);

namespace Ca\Application\Point\Delete;

use Ca\Domain\Model\Point\Id;
use Ca\Domain\Model\Point\Interfaces\PointRepositoryInterface;

final class DeleteService
{
    /**
     * @var PointRepositoryInterface
     */
    private $pointRepository;

    public function __construct(PointRepositoryInterface $pointRepository)
    {
        $this->pointRepository = $pointRepository;
    }

    public function handle(DeleteRequest $request): void
    {
        $this->pointRepository->delete(new Id($request->id()));
    }
}
