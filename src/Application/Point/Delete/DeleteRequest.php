<?php

declare (strict_types = 1);

namespace Ca\Application\Point\Delete;

final class DeleteRequest
{
    /**
     * @var int
     */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function id(): int
    {
        return $this->id;
    }
}
