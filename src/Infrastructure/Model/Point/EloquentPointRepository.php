<?php

declare (strict_types = 1);

namespace Ca\Infrastructure\Model\Point;

use Ca\Domain\Model\Point\Exceptions\PointNotFoundException;
use Ca\Domain\Model\Point\Id;
use Ca\Domain\Model\Point\Interfaces\PointRepositoryInterface;
use Ca\Domain\Model\Point\Point;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class EloquentPointRepository implements PointRepositoryInterface
{

    public function nextId(): Id
    {
        return new Id((int) EloquentPointModel::max('id') + 1);
    }

    public function save(Point $point): void
    {
        try {
            $model = $this->ofId($point->id());
        } catch (PointNotFoundException $e) {
            $model     = new EloquentPointModel();
            $model->id = $point->id()->value();
        }

        $model->location   = $point->location();
        $model->type       = $point->type();
        $model->size       = $point->size();
        $model->time       = $point->time();
        $model->activity   = $point->activity();
        $model->alignment  = $point->alignment();
        $model->additional = $point->additional();

        $model->save();
    }

    public function ofId(Id $id): Point
    {
        try {
            $model = $this->getModelById($id->value());
        } catch (ModelNotFoundException $e) {
            throw PointNotFoundException::ofId($id);
        }

        return $this->buildPoint($model);
    }

    /**
     * @return Point[]
     */
    public function all(): array
    {
        $models = EloquentPointModel::all();
        $points = [];
        foreach ($models as $model) {
            $points[] = $this->buildPoint($model);
        }

        return $points;
    }

    /**
     * @throws PointNotFoundException
     */
    public function delete(Id $id): void
    {
        try {
            $model = $this->getModelById($id->value());
        } catch (ModelNotFoundException $e) {
            throw PointNotFoundException::ofId($id);
        }

        $model->delete();
    }

    /**
     * @throws ModelNotFoundException
     */
    private function getModelById(int $id): EloquentPointModel
    {
        return EloquentPointModel::where('id', '=', $id)->firstOrFail();
    }

    private function buildPoint(EloquentPointModel $model): Point
    {
        return new Point(
            new Id($model->id),
            $model->location,
            $model->type,
            $model->size,
            $model->time,
            $model->activity,
            $model->alignment,
            $model->additional
        );
    }
}
