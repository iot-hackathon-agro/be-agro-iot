<?php
declare (strict_types = 1);

namespace Ca\Infrastructure\Model\Point;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer           $id
 * @property string            $location
 * @property string            $type
 * @property string            $size
 * @property DateTimeInterface $time
 * @property string            $activity
 * @property string            $alignment
 * @property string|null       $additional
 *
 */
class EloquentPointModel extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'   => 'integer',
        'time' => 'datetime',
    ];

    protected $table = 'points';
}
