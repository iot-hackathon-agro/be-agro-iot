<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    public function up()
    {
        Schema::create('points', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location');
            $table->string('type');
            $table->string('size');
            $table->dateTime('time');
            $table->string('activity');
            $table->string('alignment');
            $table->text('additional')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
