<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/points', 'Points\Index');
$router->get('/demo/{id}', 'Points\Demo');
$router->post('/points', 'Points\Store');
$router->delete('/points/{id}', 'Points\Delete');

//$router->put('/points/{id}', 'Points\Update');
